Project was created as a .NET Console Application with C#

Assumptions made:
1. Room must be 1m x 1m or bigger up to the maximum integer value.
2. Starting position must be inside of the room.
3. Letters in input are case sensitive.

Ways to improve:
1. The feedback to the user for a failed input validation doesnt return a detailed error. If this is needed, the regex validation should 
be switched out for a more manual validation logic where we can identify the exact error. The decision to use regex was based on keeping the logic
simple since we can specify the format in the console window to the user.
2. The feedback for a failed run only returns the position the car was heading for and what direction it was going when hitting the wall. 
The logic could easily be extended to return more accurate information if needed, for example the last command executed etc by modifying the foreach where commands are
executed and changing the resultclass.
3. If the project were to grow dependency injection should be implemented to improve performance.

Time spent in sessions:
1. 15 minutes
2. 2 h 57 min
3. 1 h 50 min
Total: ~ 5 hours