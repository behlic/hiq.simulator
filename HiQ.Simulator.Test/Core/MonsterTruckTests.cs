﻿using System.Collections.Generic;
using HiQ.Simulator.Core;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HiQ.Simulator.Test.Core
{
	[TestClass]
	public class MonsterTruckTests
	{
		private readonly MonsterTruck _monsterTruck;

		public MonsterTruckTests()
		{
			_monsterTruck = new MonsterTruck();
		}

		[TestMethod]
		public void Drive_ValidRun_ReturnsSuccessful()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Forward
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsTrue(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(1, result.Positioning.PositionY);
			Assert.AreEqual(Direction.North, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_TurnLeftWhenNorth_ReturnsHeadingWest()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Left

			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsTrue(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(0, result.Positioning.PositionY);
			Assert.AreEqual(Direction.West, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_TurnRightWhenNorth_ReturnsHeadingEast()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Right

			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsTrue(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(0, result.Positioning.PositionY);
			Assert.AreEqual(Direction.East, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_TurnRightInACircle_ReturnsHeadingSameDirection()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Right,
				Command.Right,
				Command.Right,
				Command.Right
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsTrue(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(0, result.Positioning.PositionY);
			Assert.AreEqual(Direction.North, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_TurnLeftInACircle_ReturnsHeadingSameDirection()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Left,
				Command.Left,
				Command.Left,
				Command.Left
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsTrue(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(0, result.Positioning.PositionY);
			Assert.AreEqual(Direction.North, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_InValidRun_ReturnsUnsuccessful()
		{
			var room = CreateRoom(5, 5);
			var startingPosition = CreateStartingPosition(0, 1, Direction.East);

			var commands = new List<Command>()
			{
				Command.Forward,
				Command.Forward,
				Command.Right,
				Command.Forward,
				Command.Forward,
				Command.Back,
				Command.Right,
				Command.Forward,
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsFalse(result.Success);
			Assert.AreEqual(2, result.Positioning.PositionX);
			Assert.AreEqual(-1, result.Positioning.PositionY);
			Assert.AreEqual(Direction.South, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_OutOfBounds_ReturnsUnsuccessful()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.North);

			var commands = new List<Command>()
			{
				Command.Forward,
				Command.Forward,
				Command.Forward,
				Command.Forward,
				Command.Forward,
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsFalse(result.Success);
			Assert.AreEqual(0, result.Positioning.PositionX);
			Assert.AreEqual(5, result.Positioning.PositionY);
			Assert.AreEqual(Direction.North, result.Positioning.Direction);
		}

		[TestMethod]
		public void Drive_OutOfBoundsAndBackIn_ReturnsUnsuccessful()
		{
			var room = CreateRoom(4, 4);
			var startingPosition = CreateStartingPosition(0, 0, Direction.West);

			var commands = new List<Command>()
			{
				Command.Forward,
				Command.Right,
				Command.Right,
				Command.Forward,
			};

			var result = _monsterTruck.Drive(room, startingPosition, commands);

			Assert.IsFalse(result.Success);
			Assert.AreEqual(-1, result.Positioning.PositionX);
			Assert.AreEqual(0, result.Positioning.PositionY);
			Assert.AreEqual(Direction.West, result.Positioning.Direction);
		}

		private static Position CreateStartingPosition(int x, int y, Direction direction)
		{
			return new Position
			{
				PositionX = x,
				PositionY = y,
				Direction = direction
			};
		}

		private static Room CreateRoom(int width, int length)
		{
			return new Room
			{
				Width = width,
				Length = length
			};
		}
	}
}
