﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HiQ.Simulator.Helpers;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HiQ.Simulator.Test.Helpers
{
	[TestClass]
	public class InputHelperTests
	{
		private readonly InputHelper _inputHelper;

		public InputHelperTests()
		{
			_inputHelper = new InputHelper();
		}

		[TestMethod]
		public void ValidateSizeInput_ValidInput_ReturnsTrue()
		{
			var listOfValidInput = GetListOfValidSizeInput();

			foreach (var input in listOfValidInput)
			{
				var result = _inputHelper.ValidateSizeInput(input);

				Assert.IsTrue(result);
			}
		}

		[TestMethod]
		public void ValidateSizeInput_InvalidInput_ReturnsFalse()
		{
			var listOfInvalidInput = GetListOfInvalidSizeInput();

			foreach (var input in listOfInvalidInput)
			{
				var result = _inputHelper.ValidateSizeInput(input);

				Assert.IsFalse(result);
			}
		}

		[TestMethod]
		public void ValidatePositionAndHeadingInput_ValidInput_ReturnsTrue()
		{
			var listOfValidInput = GetListOfValidPositionAndHeadingInput();

			var room = GetRoom(1000, 1000);

			foreach (var input in listOfValidInput)
			{
				var result = _inputHelper.ValidatePositionAndHeadingInput(input, room);

				Assert.IsTrue(result);
			}
		}

		[TestMethod]
		public void ValidatePositionAndHeadingInput_InvalidInput_ReturnsFalse()
		{
			var listOfInvalidInput = GetListOfInvalidPositionAndHeadingInput();
			var room = GetRoom(1000, 1000);

			foreach (var input in listOfInvalidInput)
			{
				var result = _inputHelper.ValidatePositionAndHeadingInput(input, room);

				Assert.IsFalse(result);
			}
		}

		[TestMethod]
		public void ValidateCommandsInput_ValidInput_ReturnsTrue()
		{
			const string input = "FFFRFFLBB";

			var result = _inputHelper.ValidateCommandsInput(input);

			Assert.IsTrue(result);
		}

		[TestMethod]
		public void ValidateCommandsInput_InvalidInput_ReturnsFalse()
		{
			const string input = "NX3";

			var result = _inputHelper.ValidateCommandsInput(input);

			Assert.IsFalse(result);
		}

		[TestMethod]
		public void GetRoomFromInput_ValidInput_ReturnsExpectedRoom()
		{
			const string input = "30 50";

			var result = _inputHelper.GetRoomFromInput(input);

			Assert.IsTrue(result != null);
			Assert.AreEqual(30, result.Width);
			Assert.AreEqual(50, result.Length);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GetRoomFromInput_InvalidInput_ReturnsArgumentException()
		{
			_inputHelper.GetRoomFromInput(null);
		}

		[TestMethod]
		public void GetPositioningFromInput_ValidInput_ReturnsExpectedPositioning()
		{
			const string input = "30 50 N";

			var result = _inputHelper.GetPositioningFromInput(input);

			Assert.IsTrue(result != null);
			Assert.AreEqual(30, result.PositionX);
			Assert.AreEqual(50, result.PositionY);
			Assert.AreEqual(Direction.North, result.Direction);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GetPositioningFromInput_InvalidInput_ReturnsArgumentException()
		{
			_inputHelper.GetPositioningFromInput(null);
		}

		[TestMethod]
		public void GetCommandsFromInput_ValidInput_ReturnsExpectedCommands()
		{
			const string input = "FRLB";

			var result = _inputHelper.GetCommandsFromInput(input);

			Assert.IsTrue(result != null && result.Any());
			Assert.AreEqual(Command.Forward, result[0]);
			Assert.AreEqual(Command.Right, result[1]);
			Assert.AreEqual(Command.Left, result[2]);
			Assert.AreEqual(Command.Back, result[3]);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GetCommandsFromInput_InvalidInput_ReturnsArgumentException()
		{
			_inputHelper.GetCommandsFromInput(null);
		}

		private static IEnumerable<string> GetListOfInvalidSizeInput()
		{
			return new List<string>
			{
				string.Empty,
				"NX324",
				"   ",
				"x  3",
				"-1 230",
				"2 2 2",
				"0 0",
				"0 1"
			};
		}

		private static IEnumerable<string> GetListOfValidSizeInput()
		{
			return new List<string>
			{
				"3 3",
				"1 30",
				"9999 9999",
				"200 100",
				"1 1"
			};
		}

		private static IEnumerable<string> GetListOfValidPositionAndHeadingInput()
		{
			return new List<string>
			{
				"2 3 N",
				"30 30 S",
				"0 0 W",
				"1 1 E",
				"1 1000 N"
			};
		}

		private static IEnumerable<string> GetListOfInvalidPositionAndHeadingInput()
		{
			return new List<string>
			{
				string.Empty,
				"1 30 n",
				"1 1 X",
				"200  100 N",
				"1 1  E",
				"999999 9 N"
			};
		}

		private Room GetRoom(int width, int length)
		{
			return new Room()
			{
				Width = width,
				Length = length
			};
		}
	}
}
