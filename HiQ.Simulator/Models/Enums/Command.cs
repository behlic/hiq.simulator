﻿namespace HiQ.Simulator.Models.Enums
{
	public enum Command
	{
		Forward,
		Back,
		Left,
		Right
	}
}
