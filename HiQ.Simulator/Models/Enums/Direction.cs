﻿namespace HiQ.Simulator.Models.Enums
{
	public enum Direction
	{
		North,
		South,
		West,
		East
	}
}
