﻿namespace HiQ.Simulator.Models
{
	public class SimulationResult
	{
		public bool Success { get; set; }
		public Position Positioning { get; set; }
	}
}
