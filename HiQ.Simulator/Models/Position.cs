﻿using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator.Models
{
	public class Position
	{
		public int PositionY { get; set; }
		public int PositionX { get; set; }
		public Direction Direction { get; set; }
	}
}
