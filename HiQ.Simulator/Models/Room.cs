﻿namespace HiQ.Simulator.Models
{
	public class Room
	{
		public int Width { get; set; }
		public int Length { get; set; }
	}
}
