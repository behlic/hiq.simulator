﻿using System;
using System.Collections.Generic;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator.Helpers
{
	public class SimulationHelper
	{
		private readonly InputHelper _inputHelper;

		public SimulationHelper()
		{
			_inputHelper = new InputHelper();
		}

		public Room GetRoom()
		{
			var validSize = false;
			var room = new Room();

			while (!validSize)
			{
				Console.WriteLine("Enter the size of the room with the format x y (example 4 4, room width and length must be above zero): ");
				var sizeInput = Console.ReadLine();

				if (!_inputHelper.ValidateSizeInput(sizeInput))
				{
					Console.WriteLine("Roomsize input was not valid.");
					continue;
				}

				room = _inputHelper.GetRoomFromInput(sizeInput);
				validSize = true;
			}

			return room;
		}

		public Position GetStartingPosition(Room room)
		{
			var validPositionAndHeading = false;
			var startingPosition = new Position();

			while (!validPositionAndHeading)
			{
				Console.WriteLine("Enter starting position and heading of the RC car with the format x y heading (example 0 0 N, accepted headings are N, S, W, E and position must be inside of the room): ");
				var positionAndHeadingInput = Console.ReadLine();

				if (!_inputHelper.ValidatePositionAndHeadingInput(positionAndHeadingInput, room))
				{
					Console.WriteLine("Positioning input was not valid.");
					continue;
				}

				startingPosition = _inputHelper.GetPositioningFromInput(positionAndHeadingInput);
				validPositionAndHeading = true;
			}

			return startingPosition;
		}

		public List<Command> GetCommands()
		{
			var validCommands = false;
			var commands = new List<Command>();

			while (!validCommands)
			{
				Console.WriteLine("Enter the commands to execute in order (example FFRBL, valid commands are F, B, R, L): ");
				var commandsInput = Console.ReadLine();

				if (!_inputHelper.ValidateCommandsInput(commandsInput))
				{
					Console.WriteLine("Commands input was not valid.");
					continue;
				}

				commands = _inputHelper.GetCommandsFromInput(commandsInput);
				validCommands = true;
			}

			return commands;
		}
	}
}
