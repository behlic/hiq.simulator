﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator.Helpers
{
	public class InputHelper
	{
		public bool ValidateSizeInput(string input)
		{
			return IsValidRegexForSizeInput(input);
		}

		public bool ValidatePositionAndHeadingInput(string input, Room room)
		{
			return IsValidRegexForPositionAndHeadingInput(input) && IsInsideOfRoomBoundary(input, room);
		}

		public bool ValidateCommandsInput(string input)
		{
			return IsValidRegexForCommands(input);
		}

		public Room GetRoomFromInput(string input)
		{
			try
			{
				//Split on null defaults to split on whitespace.
				var inputArray = input.Split(null);

				var width = GetIntValueFromInputArray(inputArray, 0);
				var length = GetIntValueFromInputArray(inputArray, 1);

				return new Room
				{
					Length = length,
					Width = width
				};
			}
			catch (Exception e)
			{
				throw new ArgumentException($"Roomsize input was not valid. Error: {e.Message}");
			}
		}

		public Position GetPositioningFromInput(string input)
		{
			try
			{
				//Split on null defaults to split on whitespace.
				var inputArray = input.Split(null);

				var positionX = GetIntValueFromInputArray(inputArray, 0);
				var positionY = GetIntValueFromInputArray(inputArray, 1);
				var directionValue = Convert.ToChar(inputArray[2]);

				return new Position
				{
					PositionX = positionX,
					PositionY = positionY,
					Direction = GetDirectionFromChar(directionValue)
				};
			}
			catch (Exception e)
			{
				throw new ArgumentException($"Positioning input was not valid. Error: {e.Message}");
			}
		}

		public List<Command> GetCommandsFromInput(string input)
		{
			try
			{
				return input.Select(GetCommandFromChar).ToList(); 
			}
			catch (Exception e)
			{
				throw new ArgumentException($"Command input was not valid. Error: {e.Message}");
			}
		}

		private static int GetIntValueFromInputArray(IReadOnlyList<string> input, int position)
		{
			if (int.TryParse(input[position], out var i))
			{
				return i;
			}

			throw new ArgumentException("Integer value could not be parsed from input");
		}

		private static bool IsValidRegexForSizeInput(string input)
		{
			//[1-9] for numbers less then 10, \d\d\d* for greater than or equal to 10.
			var r = new Regex(@"^(?:[1-9]|\d\d\d*)+ (?:[1-9]|\d\d\d*)+$");
			return r.IsMatch(input);
		}

		private static bool IsValidRegexForPositionAndHeadingInput(string input)
		{
			var r = new Regex(@"^([0-9])+ ([0-9])+ [NSWE]+$");
			return r.IsMatch(input);
		}

		private bool IsInsideOfRoomBoundary(string input, Room room)
		{
			var positioning = GetPositioningFromInput(input);

			return positioning.PositionX >= 0 && positioning.PositionX <= room.Width && positioning.PositionY >= 0 &&
			       positioning.PositionY <= room.Length;
		}

		private static bool IsValidRegexForCommands(string input)
		{
			var r = new Regex(@"^[FRLB]+$");
			return r.IsMatch(input);
		}

		private static Direction GetDirectionFromChar(char c)
		{
			switch (c)
			{
				case 'N':
					return Direction.North;
				case 'S':
					return Direction.South;
				case 'W':
					return Direction.West;
				case 'E':
					return Direction.East;
				default:
					throw new ArgumentException("Char could not be mapped to direction.");
			}
		}

		private static Command GetCommandFromChar(char c)
		{
			switch (c)
			{
				case 'F':
					return Command.Forward;
				case 'B':
					return Command.Back;
				case 'L':
					return Command.Left;
				case 'R':
					return Command.Right;
				default:
					throw new ArgumentException("Char could not be mapped to command.");
			}
		}
	}
}
