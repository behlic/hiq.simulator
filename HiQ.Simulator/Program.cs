﻿using System;
using System.Collections.Generic;
using HiQ.Simulator.Core;
using HiQ.Simulator.Core.Interfaces;
using HiQ.Simulator.Helpers;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator
{
	internal class Program
	{
		private static void Main()
		{
			Console.WriteLine("Welcome to the simulator for radio controlled cars v0.1");

			try
			{
				var simulationHelper = new SimulationHelper();

				var room = simulationHelper.GetRoom();

				var startingPosition = simulationHelper.GetStartingPosition(room);

				var commands = simulationHelper.GetCommands();

				var simulationResult = GetSimulationResult(room, startingPosition, commands);

				Console.WriteLine(simulationResult.Success

					? GetSuccessMessage(simulationResult)
					: GetFailureMessage(simulationResult));

				Console.ReadLine();
			}
			catch (Exception e)
			{
				Console.WriteLine($"The simulation failed because of an unexpected error. Error: {e.Message}");
			}
		}

		private static SimulationResult GetSimulationResult(Room room, Position startingPosition, List<Command> commands)
		{
			IRadioControlledCar radioControlledCar = new MonsterTruck();

			var simulationResult = radioControlledCar.Drive(room, startingPosition, commands);

			return simulationResult;
		}

		private static string GetFailureMessage(SimulationResult simulationResult)
		{
			return
				$"Car hit the wall when trying to reach the position [{simulationResult.Positioning.PositionX},{simulationResult.Positioning.PositionY}] with direction {simulationResult.Positioning.Direction}";
		}

		private static string GetSuccessMessage(SimulationResult simulationResult)
		{
			return
				$"Success, car ended at position [{simulationResult.Positioning.PositionX},{simulationResult.Positioning.PositionY}] with direction {simulationResult.Positioning.Direction}";
		}
	}
}
