﻿using System;
using System.Collections.Generic;
using HiQ.Simulator.Core.Interfaces;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator.Core
{
	public class MonsterTruck : IRadioControlledCar
	{
		private const int DistanceTravelledPerCommand = 1;

		public SimulationResult Drive(Room room, Position startingPosition, List<Command> commands)
		{
			try
			{
				var currentPosition = startingPosition;

				foreach (var command in commands)
				{
					currentPosition = ExecuteCommand(currentPosition, command);

					if (IsOutsideOfRoom(room, currentPosition))
					{
						return new SimulationResult
						{
							Success = false,
							Positioning = currentPosition
						};
					}
				}

				return new SimulationResult
				{
					Success = true,
					Positioning = currentPosition
				};
			}
			catch (Exception e)
			{
				throw new ArgumentException($"Simulation failed. Error: {e.Message}");
			}
		}

		private static Position ExecuteCommand(Position currentPosition, Command command)
		{
			switch (command)
			{
				case Command.Forward:
					currentPosition = DriveForward(currentPosition);
					break;
				case Command.Back:
					currentPosition = DriveBack(currentPosition);
					break;
				case Command.Left:
					currentPosition = TurnLeft(currentPosition);
					break;
				case Command.Right:
					currentPosition = TurnRight(currentPosition);
					break;
				default:
					throw new ArgumentException("Invalid command");
			}

			return currentPosition;
		}

		private static bool IsOutsideOfRoom(Room room, Position currentPosition)
		{
			return currentPosition.PositionY < 0 || currentPosition.PositionY > room.Length || currentPosition.PositionX < 0 || currentPosition.PositionX >= room.Width;
		}

		private static Position TurnRight(Position currentPosition)
		{
			switch (currentPosition.Direction)
			{
				case Direction.North:
					currentPosition.Direction = Direction.East;
					break;
				case Direction.South:
					currentPosition.Direction = Direction.West;
					break;
				case Direction.West:
					currentPosition.Direction = Direction.North;
					break;
				case Direction.East:
					currentPosition.Direction = Direction.South;
					break;
				default:
					throw new ArgumentException("Invalid direction.");
			}

			return currentPosition;
		}

		private static Position TurnLeft(Position currentPosition)
		{
			switch (currentPosition.Direction)
			{
				case Direction.North:
					currentPosition.Direction = Direction.West;
					break;
				case Direction.South:
					currentPosition.Direction = Direction.East;
					break;
				case Direction.West:
					currentPosition.Direction = Direction.South;
					break;
				case Direction.East:
					currentPosition.Direction = Direction.North;
					break;
				default:
					throw new ArgumentException("Invalid direction.");
			}

			return currentPosition;
		}

		private static Position DriveBack(Position currentPosition)
		{
			switch (currentPosition.Direction)
			{
				case Direction.North:
					currentPosition.PositionY -= DistanceTravelledPerCommand;
					break;
				case Direction.South:
					currentPosition.PositionY += DistanceTravelledPerCommand;
					break;
				case Direction.West:
					currentPosition.PositionX += DistanceTravelledPerCommand;
					break;
				case Direction.East:
					currentPosition.PositionX -= DistanceTravelledPerCommand;
					break;
				default:
					throw new ArgumentException("Invalid direction.");
			}

			return currentPosition;
		}

		private static Position DriveForward(Position currentPosition)
		{
			switch (currentPosition.Direction)
			{
				case Direction.North:
					currentPosition.PositionY += DistanceTravelledPerCommand;
					break;
				case Direction.South:
					currentPosition.PositionY -= DistanceTravelledPerCommand;
					break;
				case Direction.West:
					currentPosition.PositionX -= DistanceTravelledPerCommand;
					break;
				case Direction.East:
					currentPosition.PositionX += DistanceTravelledPerCommand;
					break;
				default:
					throw new ArgumentException("Invalid direction.");
			}

			return currentPosition;
		}
	}
}
