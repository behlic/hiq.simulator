﻿using System.Collections.Generic;
using HiQ.Simulator.Models;
using HiQ.Simulator.Models.Enums;

namespace HiQ.Simulator.Core.Interfaces
{
	public interface IRadioControlledCar
	{
		SimulationResult Drive(Room room, Position startingPosition, List<Command> commands);
	}
}
